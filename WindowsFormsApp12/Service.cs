﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace WindowsFormsApp12
{
    public class Service
    {
        //функции
        public String sF1;
        public String sF2;
        public String sF3;
        public String sF11;
        public String sF12;
        public String sF13;
        public String sF21;
        public String sF22;
        public String sF23;
        public String sF31;
        public String sF32;
        public String sF33;
        String str;
        double[][] d, dx1, dx2, dx3;
        public Service(int n)
        {
            d = new double[n][];
            dx1 = new double[n][];
            dx2 = new double[n][];
            dx3 = new double[n][];
            for (int i = 0; i < n; i++)
            {
                d[i] = new double[n];
                dx1[i] = new double[n];
                dx2[i] = new double[n];
                dx3[i] = new double[n];
            }
        }

        public void clearMatrix(int n)
        {
            d = new double[n][];
            dx1 = new double[n][];
            dx2 = new double[n][];
            dx3 = new double[n][];
            for (int i = 0; i < n; i++)
            { 
                d[i] = new double[n];
                dx1[i] = new double[n];
                dx2[i] = new double[n];
                dx3[i] = new double[n];
            }

        }


        double comp(String s, Vector x)
        {
            String sF = s;
            sF = sF.Replace("x0", Convert.ToString(x.a[0]));
            sF = sF.Replace("x1", Convert.ToString(x.a[1]));
            if (x.n == 3) {
                sF = sF.Replace("x2", Convert.ToString(x.a[2]));
            }
            sF = sF.Replace("sqrt", "Math.Sqrt");
            sF = sF.Replace("sin", "Math.Sin");
            sF = sF.Replace("cos", "Math.Cos");
            sF = sF.Replace("log", "Math.Log");
            sF = sF.Replace("exp", "Math.Exp");
            sF = sF.Replace(",", ".");
            sF = sF.Replace("−", "-");
            sF = sF.Replace("−-", "+");
            sF = sF.Replace("--", "+");
            sF = sF.Replace(",", ".");
            sF = sF.Replace("*-", "*(-1)*");
            Calculator cs = new Calculator();
            Double res = cs.Calc(sF);
            return res;

        }
      
        double[][] addition(double[][] mat, int n, int m, int n2)
        {
            double[][] b = mat;
            double[][] c;
            c = new double[n2 - 1][];
            for (int i = 0; i < n2 - 1; i++)
            {
                c[i] = new double[n2 - 1];
                for (int j = 0; j < n2 - 1; j++)
                {
                    if (i < n && j < m)
                    {
                        c[i][j] = b[i][j];
                    }
                    else if (i >= n && j < m)
                    {
                        c[i][j] = b[i + 1][j];
                    }
                    else if (i >= n && j >= m)
                    {
                        c[i][j] = b[i + 1][j + 1];
                    }
                    else if (i < n && j >= m)
                    {
                        c[i][j] = b[i][j + 1];
                    }

                }
            }
            return c;
        }


        double det(double[][] a, int n)
        {
            if (n == 2)
            {
                return a[0][0] * a[1][1] - a[0][1] * a[1][0];
            }
            else
            {
                double sum = 0;
                for (int i = 0; i < n; i++)
                {
                    sum = sum + Math.Pow(-1, i) * a[0][i] * det(addition(a, 0, i, n), n-1);
                }
                return sum;
            }

        }



        public double[] compMatrVsVector(double[][] d, double[] x, int n)
        {
            double[] c = new double[n];
            for(int i = 0; i<n; i++)
            {
                c[i] = 0;
                for(int j = 0; j<n; j++)
                {
                    c[i] = c[i] + d[i][j] * x[j];
                }
            }

            return c;
        }


        public double[][] compVectorVsTrVector(double[] x1, double[] x2, int n)
        {
            double[][] res = new double[n][];
            for(int i = 0; i<n; i++)
            {
                res[i] = new double[n];
            }
            for(int i = 0; i<n; i++)
            {
                for(int j = 0; j< n; j++)
                {
                    res[i][j] = x1[i] * x2[j];
                }
            }
            return res;
        }



        public double[][] recountBroiden(double[][] J, Vector Xold, Vector Xnew, int n)
        {
            double[] f = new double[n];
            double[] dx = new double[n];
            for (int i = 0; i < n; i++)
            {
                dx[i] = Xnew.a[i] - Xold.a[i];
            }
            f[0] = comp(sF1, Xnew) - comp(sF1, Xold);
            f[1] = comp(sF2, Xnew) - comp(sF2, Xold);
            if (n == 3)
            {
                f[2] = comp(sF3, Xnew) - comp(sF3, Xold);
            }

            double[] dl= new double[n];
            double[][] dJ = new double[n][];
            dl = compMatrVsVector(J, dx, n);
            for(int i = 0; i<n; i++)
            {
                dl[i] = f[i] - dl[i];
                dJ[i] = new double[n];
            }

            dJ = compVectorVsTrVector(dl, dx, n);
            double r = 0;

            for(int i = 0; i<n; i++)
            {
                r = r + dx[i] * dx[i];
            }

            for(int i = 0; i<n; i++)
            {
                for(int j = 0; j<n; j++)
                {
                    J[i][j] = J[i][j] + (dJ[i][j] / r);
                }
            }
            return J;
        }












        //public String method_Nuotona(Vector x, double e, int n)
        //{
        //        double Fold1=0;
        //        double Fold2=0;
        //        double Fold3=0;
        //        double Fnew1=0;
        //        double Fnew2=0;
        //        double Fnew3=0;
        //    int k = 0;
        //    //str = "Начальная точка:\nx0 = " + Convert.ToString(x.a[0]) + "  x1 = " + Convert.ToString(x.a[1]);
        //    if (n == 3)
        //    {
        //        str = str + "  x2 = " + Convert.ToString(x.a[2]);
        //    }
        //    str = str + "\n";
        //    while (true)
        //    {

        //        d[0][0] = comp(sF11, x);
        //        d[0][1] = comp(sF12, x);
        //        d[1][0] = comp(sF21, x);
        //        d[1][1] = comp(sF22, x);
        //        dx1[0][0] = -comp(sF1, x);
        //        dx1[1][0] = -comp(sF2, x);
        //        dx1[0][1] = comp(sF12, x);
        //        dx1[1][1] = comp(sF22, x);
        //        dx2[0][1] = -comp(sF1, x);
        //        dx2[1][1] = -comp(sF2, x);
        //        dx2[0][0] = comp(sF11, x);
        //        dx2[1][0] = comp(sF21, x);
        //        if (n == 3)
        //        {
        //            d[0][2] = comp(sF13, x);
        //            d[1][2] = comp(sF23, x);
        //            d[2][2] = comp(sF33, x);
        //            d[2][1] = comp(sF32, x);
        //            d[2][0] = comp(sF31, x);
        //            dx1[0][2] = comp(sF13, x);
        //            dx1[1][2] = comp(sF23, x);
        //            dx1[2][2] = comp(sF33, x);
        //            dx1[2][1] = comp(sF32, x);
        //            dx1[2][0] = -comp(sF3, x);
        //            dx2[0][2] = comp(sF13, x);
        //            dx2[1][2] = comp(sF23, x);
        //            dx2[2][2] = comp(sF33, x);
        //            dx2[2][1] = -comp(sF3, x);
        //            dx2[2][0] = comp(sF31, x);
        //            dx3[0][0] = comp(sF11, x);
        //            dx3[0][1] = comp(sF12, x);
        //            dx3[0][2] = -comp(sF1, x);
        //            dx3[1][0] = comp(sF21, x);
        //            dx3[1][1] = comp(sF22, x);
        //            dx3[1][2] = -comp(sF2, x);
        //            dx3[2][0] = comp(sF31, x);
        //            dx3[2][1] = comp(sF32, x);
        //            dx3[2][2] = -comp(sF3, x);
        //        }
                

        //        double detD = det(d, n);
        //        if (detD == 0)
        //            return str;
        //        double detDx1 = det(dx1, n);
        //        double detDx2 = det(dx2, n);
        //        double detDx3;
        //        if (n == 3)
        //        {
        //            detDx3 = det(dx3, n);
        //            x.a[2] = x.a[2] + (detDx3 / detD);
        //        }
        //        x.a[0] = x.a[0] + ( detDx1/detD);
        //        x.a[1] = x.a[1] + (detDx2/detD);
        //        Fnew1 = comp(sF1, x);
        //        Fnew2 = comp(sF2, x);
        //        //str = str + "Итерация: " + k.ToString() + "\nx0 = " + Convert.ToString(x.a[0]) + "  x1 = " + Convert.ToString(x.a[1]);
        //        double r = Math.Sqrt(Math.Pow(Fnew1 - Fold1, 2) + Math.Pow(Fnew2-Fold2, 2));
        //        if (n == 3)
        //        {
        //            Fnew3 = comp(sF3, x);
        //            //str = str + "  x2 = " + Convert.ToString(x.a[2]);
        //            r = Math.Sqrt(Math.Pow(Fnew1 - Fold1, 2) + Math.Pow(Fnew2 - Fold2, 2) + Math.Pow(Fnew3 - Fold3, 2));
        //        }
        //        str = str + "\n";
        //        if (r <= e || k > 9)
        //            break;
        //        k++;
        //        Fold1 = -dx2[0][1];
        //        Fold2 = -dx2[1][1];
        //        if (n == 3)
        //        {
        //            Fold3 = -dx2[2][1];
        //        }
        //    }
        //    return str;
        //}




        public String method_Broidena(Vector x, double e, int n)
        {
            int k = 0;
            double[] xOld = new double[n];
            double x1 = 0;
            double x2 = 0;
            double x3 = 0;
            str = "Начальная точка:\nx0 = " + Convert.ToString(x.a[0]) + "  x1 = " + Convert.ToString(x.a[1]);
            if (n == 3)
            {
                str = str + "  x2 = " + Convert.ToString(x.a[2]);
            }
            str = str + "\n";

            d[0][0] = comp(sF11, x);
            d[0][1] = comp(sF12, x);
            d[1][0] = comp(sF21, x);
            d[1][1] = comp(sF22, x);
            if (n == 3)
            {
                d[0][2] = comp(sF13, x);
                d[1][2] = comp(sF23, x);
                d[2][2] = comp(sF33, x);
                d[2][1] = comp(sF32, x);
                d[2][0] = comp(sF31, x);
            }


            while (true)
            {
                double Fold1 = 0;
                double Fold2 = 0;
                double Fold3 = 0;
                double Fnew1 = 0;
                double Fnew2 = 0;
                double Fnew3 = 0;


                if (k > 0)
                {
                    xOld[0] = x1;
                    xOld[1] = x2;
                    if (n == 3)
                    {
                        xOld[2] = x3;
                    }
                    Vector oldVector = new Vector(n);
                    for(int i = 0; i<n; i++)
                    {
                        oldVector.a[i] = xOld[i];
                    }
                    
                    d = recountBroiden(d, oldVector, x, n);
                    for (int i = 0; i < n; i++)
                    {
                       xOld[i] = x.a[i];
                    }
                }



                dx1[0][0] = -comp(sF1, x);
                dx1[1][0] = -comp(sF2, x);
                dx1[0][1] = d[0][1];
                dx1[1][1] = d[1][1];
                dx2[0][1] = -comp(sF1, x);
                dx2[1][1] = -comp(sF2, x);
                dx2[0][0] = d[0][0];
                dx2[1][0] = d[1][0];
                Fold1 = -dx2[0][1];
                Fold2 = -dx2[1][1];
                if (n == 3)
                {
                    dx1[0][2] = d[0][2];
                    dx1[1][2] = d[1][2];
                    dx1[2][2] = d[2][2];
                    dx1[2][0] = -comp(sF3, x);
                    Fold3 = -dx1[2][0];
                    dx1[2][1] = d[2][1];
                    dx2[0][2] = d[0][2];
                    dx2[1][2] = d[1][2];
                    dx2[2][2] = d[2][2];
                    dx2[2][0] = d[2][0];
                    dx2[2][1] = -comp(sF3, x);
                    dx3[0][0] = d[0][0];
                    dx3[0][1] = d[0][1];
                    dx3[0][2] = -comp(sF1, x);
                    dx3[1][0] = d[1][0];
                    dx3[1][1] = d[1][1];
                    dx3[1][2] = -comp(sF2, x);
                    dx3[2][0] = d[2][0];
                    dx3[2][1] = d[2][1];
                    dx3[2][2] = -comp(sF3, x);

                }
                x1 = x.a[0];
                x2 = x.a[1];
                x3 = x.a[2];
                double detD = det(d, n);
                if (detD == 0)
                    return str;
                double detDx1 = det(dx1, n);
                double detDx2 = det(dx2, n);
                double detDx3;
                if (n == 3)
                {
                    x3 = x.a[2];
                    detDx3 = det(dx3, n);
                    x.a[2] = x.a[2] + (detDx3 / detD);
                }
                x.a[0] = x.a[0] + (detDx1 / detD);
                x.a[1] = x.a[1] + (detDx2 / detD);
                str = str +"Итерация: "+ k.ToString() + "\nx0 = " + Convert.ToString(x.a[0]) + "; x1 = " + Convert.ToString(x.a[1]);
                Fnew1 = comp(sF1, x);
                Fnew2 = comp(sF2, x);
                double r = Math.Sqrt(Math.Pow(Fnew1 - Fold1, 2) + Math.Pow(Fnew2 - Fold2, 2));
                if (n == 3)
                {
                    Fnew3 = comp(sF3, x);
                    str = str + "  x2 = " + Convert.ToString(x.a[2]);
                    r = Math.Sqrt(Math.Pow(Fnew1 - Fold1, 2) + Math.Pow(Fnew2 - Fold2, 2) + Math.Pow(Fnew3 - Fold3, 2));
                }


                if (r <= e || k > 25)
                { str = str + "\n\nНорма" + Convert.ToString(r) + "<" + e + "\n"; break; }
                else { str = str + "\n\nНорма" + Convert.ToString(r) + ">" + e + "\n"; }
                k++;
            }
            return str;




        }





    }
}
