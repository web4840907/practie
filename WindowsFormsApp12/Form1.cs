﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp12
{
    public partial class Form1 : Form
    {
        string path;

        public Form1()
        {
            InitializeComponent();
        }


        public Form1(string pathApp)
        {
            InitializeComponent();
            path = pathApp;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
       
        }

        //private void методНьютонаToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    Service methods = new Service(Convert.ToInt32(button1.Text));
        //    try
        //    {
        //        methods.sF1 = textBox9.Text;
        //        methods.sF2 = textBox10.Text;
        //        methods.sF3 = textBox13.Text;
        //        methods.sF11 = textBox7.Text;
        //        methods.sF12 = textBox19.Text;
        //        methods.sF13 = textBox20.Text;
        //        methods.sF21 = textBox16.Text;
        //        methods.sF22 = textBox21.Text;
        //        methods.sF23 = textBox22.Text;
        //        methods.sF31 = textBox17.Text;
        //        methods.sF32 = textBox18.Text;
        //        methods.sF33 = textBox23.Text;
        //        Vector vec = new Vector(3);
        //        vec.a[0] = Convert.ToDouble(textBox5.Text.Replace(".", ","));
        //        vec.a[1] = Convert.ToDouble(textBox11.Text.Replace(".", ","));
        //        if (textBox14.Text != "" && Convert.ToInt32(button1.Text) == 3)
        //        {
        //            vec.a[2] = Convert.ToDouble(textBox14.Text.Replace(".", ","));
        //        }               
        //        String str = methods.method_Nuotona(vec, Convert.ToDouble(textBox8.Text.Replace(".", ",")), Convert.ToInt32(button1.Text));
        //        richTextBox1.Text ="Метод Ньютона:\n" + str;
        //    }
        //    catch(Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
            
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            if(button1.Text == "2")
            {
                button1.Text = "3";
            }
            else
            {
                button1.Text = "2";
            }
        }

        private void методБройденаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Service methods = new Service(Convert.ToInt32(button1.Text));
            try
            {
                methods.sF1 = textBox9.Text;
                methods.sF2 = textBox10.Text;
                methods.sF3 = textBox13.Text;
                methods.sF11 = textBox7.Text;
                methods.sF12 = textBox19.Text;
                methods.sF13 = textBox20.Text;
                methods.sF21 = textBox16.Text;
                methods.sF22 = textBox21.Text;
                methods.sF23 = textBox22.Text;
                methods.sF31 = textBox17.Text;
                methods.sF32 = textBox18.Text;
                methods.sF33 = textBox23.Text;
                Vector vec = new Vector(3);
                vec.a[0] = Convert.ToDouble(textBox5.Text.Replace(".", ","));
                vec.a[1] = Convert.ToDouble(textBox11.Text.Replace(".", ","));
                if (textBox14.Text != "" && Convert.ToInt32(button1.Text) == 3)
                {
                    vec.a[2] = Convert.ToDouble(textBox14.Text.Replace(".", ","));
                }
                String str = methods.method_Broidena(vec, Convert.ToDouble(textBox8.Text.Replace(".", ",")), Convert.ToInt32(button1.Text));
                richTextBox1.Text ="Метод Бройдена:\n" + str;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }



        }

        private void загрузитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                String str1 = "";
                String str2 = "";
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string pathLoading = openFileDialog1.FileName;
                    StreamReader sr = new StreamReader(pathLoading);
                    int count = 0;
                    while (sr.Peek() >= 0)
                    {
                       
                        int r = Convert.ToInt32(sr.ReadLine());
                        Service methods = new Service(r);
                        Vector vec = new Vector(3);
                        Vector vec2 = new Vector(3);
                        vec.a[0] = Convert.ToDouble(sr.ReadLine().Replace(".", ","));
                        vec.a[1] = Convert.ToDouble(sr.ReadLine().Replace(".", ","));
                        vec2.a[0] = vec.a[0];
                        vec2.a[1] = vec.a[1];
                        if (r == 3)
                        {
                            vec.a[2] = Convert.ToDouble(sr.ReadLine().Replace(".", ","));
                            vec2.a[2] = vec.a[2];
                        }
                        double eps = Convert.ToDouble(sr.ReadLine().Replace(".", ","));
                        methods.sF1 = sr.ReadLine();
                        methods.sF2 = sr.ReadLine();
                        if (r == 3)
                        {
                            methods.sF3 = sr.ReadLine();
                        }
                        methods.sF11 = sr.ReadLine();
                        methods.sF12 = sr.ReadLine();
                        if (r == 3)
                        {
                            methods.sF13 = sr.ReadLine();
                        }
                        methods.sF21 = sr.ReadLine();
                        methods.sF22 = sr.ReadLine();
                        if (r == 3)
                        {
                            methods.sF23 = sr.ReadLine();
                            methods.sF31 = sr.ReadLine();
                            methods.sF32 = sr.ReadLine();
                            methods.sF33 = sr.ReadLine();
                        }

                        str1 = methods.method_Broidena(vec, eps, r);
                        str1 = "Метод Бройдена:\n" + str1;
                        count++;
                        StreamWriter sw = new StreamWriter(path + "\\Broiden" + count + ".txt");
                        sw.WriteLine(str1);
                        sw.Close();
                       
                        if (sr.Peek() >= 0)
                            sr.ReadLine();
                    }
                    if (count == 1)
                    {
                        richTextBox1.Text = str1;
                    }
                    sr.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileStream fs = new FileStream("result.txt", FileMode.Create, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(openFileDialog1.FileName);
                    sw.WriteLine(richTextBox1.Text);
                    sw.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string help = "Загрузка файла производится по следующиму шаблону:\nРазмерность\n" +
                "x0\n" +
                "x1\n" +
                "x2\n" +
                "Точность\n" +
                "F[0]\n" +
                "F[1]\n" +
                "F[2]\n" +
                "Матрица Якоби ячейка 11\n" +
                "ячейка 12, где 1-соответствует строке, 2-соответствует столбцу\n " +
                "ячейка 13\n" +
                "ячейка 21\n" +
                "ячейка 22\n" +
                "ячейка 23\n" +
                "ячейка 31\n" +
                "ячейка 32\n" +
                "ячейка 33\n" +
                "Если размерность 2, то вводить надо без x2, F[2], ячейки 13 и т.д\n" +
                "Для подсчета нескольких функций их необходимо разделить с помощью ENTER\n" +
                "Для каждой функции произведутся вычисления методом Бройдена\n";
            help = help + "\nРаботу выполнила Гурова Анна ПИ-20-2";
            MessageBox.Show(help);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
