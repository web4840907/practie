﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp12
{
    public class Matrix
    {

        public int n;
        public double[][] a;

        public Matrix()
        {
            n = 0;
        }

        public Matrix(int number, double[][] b)
        {
            n = number;
            a = new double[n][];
            for (int i = 0; i < n; i++)
            {
                a[i] = new double[n];
                for (int j = 0; j < n; j++)
                {
                    a[i][j] = b[i][j];
                }
            }
        }




        ~Matrix()
        {
        }

    }
}
