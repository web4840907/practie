﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom;

namespace WindowsFormsApp12
{
    public class Calculator
    {
        // Создадим экземпляр генератора и компилятора C#-кода
        public Microsoft.CSharp.CSharpCodeProvider CodeProvider { get; } = new Microsoft.CSharp.CSharpCodeProvider();
        // Зададим опции компиляции
        public System.CodeDom.Compiler.CompilerParameters CompilerParameters { get; } = new System.CodeDom.Compiler.CompilerParameters()
        {
            // Нам не нужен исполняемый файл
            GenerateExecutable = false,
            // Выходной файл должен располагаться в памяти
            GenerateInMemory = false
        };

        public double Calc(string Expression)
        {
            try
            {
                // Откомпилируем наше выражение, которое будет располагаться внутри метода Calc класса Calculator
                System.CodeDom.Compiler.CompilerResults compilerResult = CodeProvider.CompileAssemblyFromSource(CompilerParameters, $"using System; static class Calculator {{ public static double Calc() {{ return {Expression}; }} }}");
                // Получим сборку
                System.Reflection.Assembly assembly = compilerResult.CompiledAssembly;
                // Достанем из сборки нужный нам тип Calculator, из него - метод Calc и таки запустим его, приведя результат исполнения к double
                return (double)assembly.DefinedTypes.First(x => x.Name == "Calculator").GetMethods().First(x => x.Name == "Calc").Invoke(null, null);
            }
            catch
            {
                // Выражение некорректно и не может быть вычислено 
                throw new Exception();
            }
        }
    }
}
