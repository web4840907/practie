grammar Calculator;

/// Starting rule
compilationUnit : expression+;

/// Parser Rules

// ��������� ���� ���������
expression 
        // ���� ����� �� ���������� ���� ��������� � �������
    :   LBRACKET expression RBRACKET                        # Brackets

        // ����� ���� single-�������
    |   statement=(SIN | COS | ROUND) expression            # Function

        // ���������� � �������
        // <assoc=right> ���������� ������ ������ ������ ��������� ������ ������
    |   expression statement=POW expression <assoc=right>   # Pow

        // ������� � ���������
    |   expression statement=(MUL | DIV | MOD) expression   # MulDiv

        // �������� � ���������
    |   expression statement=(SUB | ADD) expression         # SubAdd

        // ������������ ���� ���������
    |   constant=(PI | E)                                   # Constant

        // ������ �������� �������� (��������: 3)
    |   VAL                                                 # Val
    ;


/// Lexer Rules

// ���������, ������� �� ����� ����� ������������ 
// ��� ����������� ������ ����������� ���������
fragment DIGIT      : [0-9];
fragment NUMBER     : DIGIT+;
fragment DOT        : '.';
fragment COMMA      : ',';
fragment SEPARATOR  : (DOT | COMMA);

// ��������, ������������ � �������
E                   : 'e';
PI                  : 'pi';
ADD                 : '+';
SUB                 : '-';
MUL                 : '*';
DIV                 : '/';
MOD                 : '%';
POW                 : '^';
COS                 : 'cos';
SIN                 : 'sin';
LBRACKET            : '(';
RBRACKET            : ')';
ROUND               : 'round';
VAL                 : NUMBER((SEPARATOR)NUMBER)?;

// WhiteSpace, ������� ��� �� �����-�� � �����)
WS                  : (' '|'\r'|'\n'|'\t') -> skip;