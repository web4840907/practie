﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp12
{
    public class Vector
    {
        public double[] a;
        public int n;

        public Vector(int number)
        {
            n = number;
            a = new double[number];
        }

    }
}
